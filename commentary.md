---
title: Representing and understanding transient dynamics in the terrestrial carbon cycle
author: Carlos A. Sierra
affiliation: Max Planck Institute for Biogeochemistry
...

# Abstract
Models representing exchange of carbon and other biogeochemical elements between the atmosphere and the terrestrial biosphere include a large variety of processes and mechanisms, and have increased in complexity in the last decades. These models are no exception of the simulation vs. understanding conundrum previously articulated for climate models (Held 2005, Bull Am Math Soc 86: 1609-1614); which states that increasing detail in process representation in models, and the simulations they produce, hinders understanding of holistic system behavior. However, recent progress in theory of the mathematical representation of carbon cycle in ecosystems may help to provide a general framework for the qualitative understanding of models without compromising detail in process representation. Here I 1) briefly review recent ideas on the theory of transient dynamics of the terrestrial carbon cycle and its matrix representation, 2) show that these ideas can be further generalized in the mathematical concept of non-autonomous compartmental systems, and 3) provide ideas on how this framework can be used to develop models that can be more easily understood by a broader scientific community and can be more efficiently implemented in modern computer languages.

# Introduction
<!-- Provides an introduction of the history of previous and current ideas on modeling the terrestrial carbon cycle. Present the main objectives of the manuscript and its organization. -->
The global carbon cycle consists of a variety of complex processes that requires the development of models for its understanding and prediction of future dynamics. The history of carbon cycle modeling is rich, and for many decades different models with different levels of complexity have been proposed. The terrestrial carbon cycle in particular, has been described with a large numbers of models that vary in their degree of complexity, number of processes represented, scales of application, and implementation in different computing languages. It is therefore difficult to compare different models, and for this reason researches resort to comparisons of model output to assess the performance of models, but not necessarily on the concepts and the mathematics directly implemented in the model.

The problem does not necessarily belong to carbon cycle models, but climate and Earth system models in general. For instance, Held2005 describes a conundrum in climate modeling for which the degree of complexity and sophistication of model implementations hinders understanding of general patterns of atmospheric circulation and climate dynamics. On the one hand it is important to implement detailed representations of relevant processes in models, but on the other hand, as the level of detail increases in a model, so does the uncertainty in parameterizations and predictions as well as our inability to understand general system dynamics of the overrepresented system (Bolin1983Scope16, Held2005, Held2014).

One potential way to deal with this complexity-understanding conundrum is through the mathematical representation of models as dynamical systems, which can help to increase our level of understanding of general system dynamics and simultaneously allow complex representations of multiple processes. There have been recent progress in this direction through the representation of terrestrial carbon models in matrix form, which we can potentially use to take full advantage of the rich existing mathematical theory of dynamical systems. This would allow us to make progress not only on holistic system understanding, but also in the development of more efficient and clean codes, develop models at different levels of complexity under one overarching theory, and facilitate comparisons of competing theories implemented in different models.

In this commentary, I will review recent progress on the mathematical representation of terrestrial carbon models as dynamical systems, propose a general representation of models using the concept of compartmental systems, and outline potential new developments in theory and implementation of models in modern programing languages.

# Carbon cycle models as dynamical systems
<!-- Shows previous work representing models as dynamical systems. This implied a transition from a discrete time-step approach that produces simulations using large loops, to an understanding that this models can be expressed as systems of differential equations. -->
Models of the terrestrial carbon cycle, and Earth system models in general, fit in the general mathematical definition of a dynamical system (Lucarini, Raupach). According to Jost2005 p. 1, *"A dynamical system is a system that evolves in time through the iterated application of an underlying dynamical rule. That transition rule describes the change of the actual state in terms of itself and possibly also previous states"*. Independent on how complicated a model might be, we can think of it as a rule that is applied recursively to describe the time evolution of mass and/or energy over time. This rule in early carbon cycle models is implemented as a large loop that updates the state of the system, generally C stocks in separate compartments, in discrete time steps such as days or years. Other models implement the update rules based on sets of ordinary or partial differential equations, which also fit the definition of dynamical systems.

The set of equations generally implemented in models are based on mass balance constraints, and can be written more compactly in matrix and vector form. Bolin1983ch4Scope16 was one of the first to write a simple carbon cycle model in matrix form. However, this form of representing carbon models have gained more traction recently by the development of the traceability framework (Xia) and the transient dynamics theory (Luo) of the terrestrial carbon cycle. Within both frameworks, carbon stocks in ecosystem pools $X$ are represented as

$$
\frac{dX}{dt} = \mathbf{B} \cdot \mathbf{u} - \mathbf{A} \cdot \xi(t) \cdot \mathbf{K \cdot X}(t)
$$
where B, u, xi, K, ...

This is a very general representation of carbon cycle models, which most likely can accommodate most existing carbon cycle models. The framework can indeed be applied to better trace different components of the carbon cycle, determine time-scales of different processes, and in general better understand potential model behavior.

However, it is important to highlight that this is a linear representation of the carbon cycle, and nonlinear processes in which the rate of change of one state variable depends on the state of other variables cannot be represented. This can occur when, for example, carbon uptake from the foliage depends on stored non-structural carbohydrates or roots; or when the presence of carbon of a labile SOM pool influence the rate of more recalcitrant pools such as the case of the priming effect.

It is important to recognize that other alternative representations are possible, and that a general classification of different model structures is possible according of general characteristics of the dynamical systems.

# Classification of carbon cycle models

## Linear versus non-linear representations
Introduces the ideas of the qualitative analysis of dynamical systems, and how bifurcation theory has been fundamental in shaping the understanding of system behavior in ecology and Earth system science.

## The autonomous versus the non-autonomous case
Introduce the compact representations of models developed by Yiqi Luo's lab, the transient and non-transient traceability frameworks, and their impact in understanding carbon storage capacity. Show some limitations of the framework.

# The general representation as non-autonomous compartmental systems
Show that a more general representation of models exists that can deal with non-linearity and non-autonomy simultaneously.

# General mathematical properties of models
Show simple results on the general properties of the non-autonomous compartmental model, particularly the conditions necessary for its stability.

# Ideas on simpler implementations in modern computer languages
Show that implementing models using compact matrix representation can drastically reduce code redundancy, facilitate the understanding of implemented processes and overall system behavior, and can more easily separate the role of Earth system scientists from that of computer scientists in the process of model development.
Provide an example in Python, probably as supplementary material, on how this can be made.
