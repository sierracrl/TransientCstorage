---
title: Representing and understanding transient dynamics in the terrestrial carbon cycle
author: Carlos A. Sierra
affiliation: Max Planck Institute for Biogeochemistry
...

# Abstract
Models representing exchange of carbon and other biogeochemical elements between the atmosphere and the terrestrial biosphere include a large variety of processes and mechanisms, and have increased in complexity in the last decades. These models are no exception of the simulation vs. understanding conundrum previously articulated for climate models (Held 2005, Bull Am Math Soc 86: 1609-1614); which states that increasing detail in process representation in models, and the simulations they produce, hinders understanding of holistic system behavior. However, recent progress in theory of the mathematical representation of carbon cycle in ecosystems may help to provide a general framework for the qualitative understanding of models without compromising detail in process representation. Here I 1) briefly review recent ideas on the theory of transient dynamics of the terrestrial carbon cycle and its matrix representation, 2) show that these ideas can be further generalized in the mathematical concept of non-autonomous compartmental systems, and 3) provide ideas on how this framework can be used to develop models that can be more easily understood by a broader scientific community and can be more efficiently implemented in modern computer languages.

# Introduction
Provides an introduction of the history of previous and current ideas on modeling the terrestrial carbon cycle. Present the main objectives of the manuscript and its organization. 

# Carbon cycle models as dynamical systems
Shows previous work representing models as dynamical systems. This implied a transition from a discrete time-step approach that produces simulations using large loops, to an understanding that this models can be expressed as systems of differential equations. 

## Linear versus non-linear representations
Introduces the ideas of the qualitative analysis of dynamical systems, and how bifurcation theory has been fundamental in shaping the understanding of system behavior in ecology and Earth system science. 

## The autonomous versus the non-autonomous case
Introduce the compact representations of models developed by Yiqi Luo's lab, the transient and non-transient traceability frameworks, and their impact in understanding carbon storage capacity. Show some limitations of the framework.

# The general representation as non-autonomous compartmental systems
Show that a more general representation of models exists that can deal with non-linearity and non-autonomy simultaneously. 

# General mathematical properties of models
Show simple results on the general properties of the non-autonomous compartmental model, particularly the conditions necessary for its stability.

# Ideas on simpler implementations in modern computer languages
Show that implementing models using compact matrix representation can drastically reduce code redundancy, facilitate the understanding of implemented processes and overall system behavior, and can more easily separate the role of Earth system scientists from that of computer scientists in the process of model development. 
Provide an example in Python, probably as supplementary material, on how this can be made.

